﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] private int damage = 3;
    [SerializeField] private float radius = 6;
    [SerializeField] private Vector3 offset;
    [SerializeField] private ParticleSystem electric;
    private Animator anim;

    private ProgressBar prgBar;

    private bool built,ready;
    private float timer;
    private float coolDown = 5.0f;

    private void Start()
    {
        anim = GetComponent<Animator>();
        prgBar = ProgressBarContainer.Instance.GetProgressBar();
        prgBar.StartAction(15.0f, transform, offset);
    }

    public void Update()
    {
        if (built)
        {
            timer += Time.deltaTime;
            if (timer > coolDown)
                ready = true;
        }
    }

    public void NowBuilt()
    {
        built = ready = true;
    }

    public void LaunchAttack()
    {
        if (ready)
        {
            anim.SetTrigger("Shake");
            electric.Play();
            timer = 0;
            ready = false;
            Collider[] hits = Physics.OverlapSphere(transform.position, radius, LayerMask.GetMask("Enemy"));

            foreach (Collider c in hits)
                c.GetComponent<Enemy>().TakeDamage(damage);

            prgBar.StartAction(coolDown, transform, offset);
            AudioManager.Instance.PlayClip(3);
        }
    }
}
