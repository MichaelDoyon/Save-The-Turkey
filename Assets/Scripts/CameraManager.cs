﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraState
{
    Top = 0,
    Iso = 1,
    Intro = 2,
    Death = 3,
}

public class CameraManager : MonoSingleton<CameraManager>
{
    [SerializeField] private GameObject[] cameras;

    public void SwitchCamera(CameraState state)
    {
        foreach (GameObject go in cameras)
            go.SetActive(false);

        cameras[(int)state].SetActive(true);
    }
}
