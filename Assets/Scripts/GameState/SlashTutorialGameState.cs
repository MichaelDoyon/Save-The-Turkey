﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashTutorialGameState : BaseGameState
{
    public override void Construct()
    {
        UIManager.Instance.SetTrigger("Slash_Hit");
        CameraManager.Instance.SwitchCamera(CameraState.Iso);
        FindObjectOfType<TargetDummy>().OnBegin();
        Motor.Instance.ChangeToSlash();
        BuildManager.Instance.ChangeToSlash();
    }
}
