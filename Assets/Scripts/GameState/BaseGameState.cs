﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGameState : MonoBehaviour
{


    public virtual void Construct()
    {
        Debug.Log(this.name + " has been constructed");
    }

    public virtual void Destruct()
    {

    }

    public virtual void Transition()
    {

    }

    public virtual void UpdateState()
    {

    }
}
