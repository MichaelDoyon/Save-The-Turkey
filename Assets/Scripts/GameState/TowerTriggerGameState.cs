﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTriggerGameState : BaseGameState
{
    [SerializeField] private GameObject dummyPrefab;
    [SerializeField] private Vector3 offset = new Vector3(0.5f, 0, 0);

    private GameObject dummy,dummy2;
    private bool once = true;

    public override void Construct()
    {
        Tower t = FindObjectOfType<Tower>();

        dummy = Instantiate(dummyPrefab, t.transform.position + Vector3.forward + offset, Quaternion.identity);
        dummy2 = Instantiate(dummyPrefab, t.transform.position - Vector3.forward + offset, Quaternion.identity);
    }

    public override void UpdateState()
    {
        if (!dummy.activeSelf && !dummy2.activeSelf && once)
        {
            once = false;
            UIManager.Instance.SetTrigger("Slash_ToBuild");
        }
    }
}
