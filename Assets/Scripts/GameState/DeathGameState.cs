﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathGameState : BaseGameState
{
    public override void Construct()
    {
        CameraManager.Instance.SwitchCamera(CameraState.Death);
        UIManager.Instance.SetTrigger("Death");
    }

    public override void UpdateState()
    {
        if (Input.GetMouseButtonDown(0))
            UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }
}
