﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGameState : BaseGameState
{
    public override void Construct()
    {
        CameraManager.Instance.SwitchCamera(CameraState.Top);
        UIManager.Instance.SetTrigger("Build");
        UIManager.Instance.SetTrigger("Build_Click");
        BuildManager.Instance.ChangeToBuild();
    }
}
