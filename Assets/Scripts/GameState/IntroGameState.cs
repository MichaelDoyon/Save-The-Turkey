﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroGameState : BaseGameState
{
    public override void Construct()
    {
        CameraManager.Instance.SwitchCamera(CameraState.Intro);
    }

    public override void UpdateState()
    {
        PoolInput();
    }

    public void PoolInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            GameScene.Instance.ChangeToTutorial();
    }
}
