﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashGameState : BaseGameState
{
    public override void Construct()
    {
        UIManager.Instance.SetTrigger("Slash");
        CameraManager.Instance.SwitchCamera(CameraState.Iso);
        Motor.Instance.ChangeToSlash();
        BuildManager.Instance.ChangeToSlash();
    }
}
