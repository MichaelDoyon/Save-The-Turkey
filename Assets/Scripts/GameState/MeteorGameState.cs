﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorGameState : BaseGameState
{
    public override void Construct()
    {
        BuildManager.Instance.ChangeToMeteor();
    }
}
