﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildGameState : BaseGameState
{
    public override void Construct()
    {
        UIManager.Instance.SetTrigger("Build");
        CameraManager.Instance.SwitchCamera(CameraState.Top);
        Motor.Instance.ChangeToBuild();
        BuildManager.Instance.ChangeToBuild();
    }
}
