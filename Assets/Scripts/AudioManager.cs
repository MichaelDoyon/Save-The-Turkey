﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 0 ->
 * 1 ->
 */

public class AudioManager : MonoSingleton<AudioManager>
{
    [SerializeField] public List<AudioClip> clips;
    private AudioSource s;

    private void Start()
    {
        s = GetComponent<AudioSource>();    
    }

    public void PlayClip(int id)
    {
        float r = Random.Range(0.75f, 1.1f);
        s.pitch = r;
        s.PlayOneShot(clips[id]);
    }
}
