﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ParticleType
{
    trail = 0,
    hit = 1,
    meteor = 2,
}

public class ParticleManager : MonoBehaviour
{
    private struct PoolableParticle
    {
        public GameObject Go { set; get; }
        public ParticleType Type { set; get; }
    }

    private List<PoolableParticle> pool = new List<PoolableParticle>();
    [SerializeField] private GameObject[] particlePrefabs;

    public GameObject GetParticle(ParticleType t)
    {
        GameObject go = pool.Find(x => x.Type == t && !x.Go.activeSelf).Go;

        if (go == null)
        {
            go = Instantiate(particlePrefabs[(int)t]);
            pool.Add(new PoolableParticle() { Go = go, Type = t });
        }

        return go;
    }
}
