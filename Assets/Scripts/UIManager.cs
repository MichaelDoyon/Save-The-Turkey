﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private Animator anim;

    private void Start()
    {
        
    }

    public void SetTrigger(string t)
    {
        anim.SetTrigger(t);
    }
}
