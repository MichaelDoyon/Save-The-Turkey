﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveInfo
{
    [SerializeField] private int[] enemyIndex;
    [SerializeField] private int waitTime;

    public int[] EnemyIndex { get { return enemyIndex; } }
    public int WaitTime { get { return waitTime; } }

    private int enemySpawned = 0;

    public int CanWeSpawnNext()
    {
        if (enemySpawned >= enemyIndex.Length)
            return -1;

        return enemyIndex[enemySpawned++];
    }
}

public class SpawnManager : MonoSingleton<SpawnManager>
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private WaveInfo[] waves;
    private int waveIndex;
    private List<GameObject> pool = new List<GameObject>();
    private bool isStarted = false;
    private float waveTimer;
    private WaveInfo wave;
    private float lastSpawn;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            SpawnEnemy();

        if (isStarted)
        {
            if (Time.time - lastSpawn > 1)
            {
                lastSpawn = Time.time;
                int i = wave.CanWeSpawnNext();
                if (i == -1)
                {
                    // done spawning
                }
                else
                {
                    SpawnEnemy(i);
                }
            }

            waveTimer += Time.deltaTime;
            if (waveTimer > wave.WaitTime)
                OnNewWave();
        }
    }

    public void OnGameStart()
    {
        isStarted = true;
        waveTimer = 0;
        wave = waves[waveIndex];
    }

    public void OnNewWave()
    {
        waveTimer = 0;
        waveIndex++;
        wave = waves[waveIndex];
    }

    public void SpawnEnemy()
    {
        SpawnEnemy(0);
    }
    public void SpawnEnemy(int id)
    {
        Enemy e = GetEnemy(id).GetComponentInChildren<Enemy>();
        e.maxHitpoint = id;
        e.hitpoint = id;

        float r = 0.2f * id;
        r = Mathf.Clamp(r, 0, 1);

        e.transform.localScale = (Vector3.one * 0.75f) + Vector3.one * r;
        e.Launch();
    }
    private GameObject GetEnemy(int id)
    {
        GameObject go = pool.Find(x => !x.activeSelf);

        if (go == null)
        {
            go = Instantiate(enemyPrefab);
            pool.Add(go);
        }

        return go;
    }
}
