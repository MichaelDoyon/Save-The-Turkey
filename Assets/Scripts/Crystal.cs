﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystal : MonoBehaviour
{
    [SerializeField] private int hitpoint = 2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            TakeDamage();
            other.GetComponent<Enemy>().Destroy();
        }
    }

    private void TakeDamage()
    {
        AudioManager.Instance.PlayClip(1);
        hitpoint--;
        if (hitpoint == 0)
        {
            GameScene.Instance.ChangeToDeath();
        }
    }
}
