﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoSingleton<Motor>
{
    private const float DELTA_SPEED_TO_SPAWN_TRAIL = 0.2f;

    [SerializeField] private float speed = 3.0f;
    private Animator anim;
    private CharacterController controller;
    private Vector3 moveDelta;
    private State state;
    [SerializeField] private ParticleSystem trail;
    [SerializeField] private ParticleSystem meteor;
    [SerializeField] private ParticleSystem meteorLanding;

    private float timer;
    private Vector3 lastStatePos;
    private Vector3 lastDirection;
    private bool landed = false;

    public Vector3 LandPosition { set; get; }

    [SerializeField] private Collider fishSwordCollider;

    private enum State
    {
        build = 0,
        slash = 1,
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        state = State.build;
        meteor.Stop();
    }
    private void Update()
    {
        switch (state)
        {
            case State.build:
                UpdateBuild();
                break;

            case State.slash:
                UpdateSlash();
                break;

            default:
                Debug.Log("No state");
                break;
        }
    }
    private void UpdateBuild()
    {
        timer += Time.deltaTime;
        if (timer <= 1)
        {
            transform.position = Vector3.Lerp(lastStatePos, new Vector3(0, 15, 30),timer);
        }
    }
    private void UpdateSlash()
    {
        if (timer <= 2)
        {
            timer += Time.deltaTime;
            transform.position = Vector3.Lerp(lastStatePos, LandPosition, timer/2);
            if (timer > 2)
                OnMeteorLand();
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
                Swing();

            moveDelta = Vector3.zero;

            moveDelta.x = Input.GetAxisRaw("Horizontal");
            moveDelta.z = Input.GetAxisRaw("Vertical");
            moveDelta = moveDelta.normalized * speed;
            moveDelta = RotateWithView();

            controller.Move(moveDelta * Time.deltaTime);

            anim.SetFloat("Speed", moveDelta.sqrMagnitude);

            Collider[] c = Physics.OverlapSphere(transform.position, 2.0f, LayerMask.GetMask("Tower"));

            if (moveDelta != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveDelta), 0.15f);
                lastDirection = moveDelta;

                if (moveDelta.magnitude > (speed - DELTA_SPEED_TO_SPAWN_TRAIL) && Time.time - lastTrailSpawn > trailSpawnDelta)
                {
                    lastTrailSpawn = Time.time;
                    trail.Play();
                    AudioManager.Instance.PlayClip(0);
                }
            }

            if (c.Length > 0)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    c[0].GetComponent<Tower>().LaunchAttack();
                }
            }
        }
    }

    private float lastSwing;
    private float swingCooldown = 1.0f;
    private float lastTrailSpawn;
    private float trailSpawnDelta = 0.25f;

    public void StartSwing()
    {
        Debug.Log("StartSwing");
        fishSwordCollider.enabled = true;
    }
    public void EndSwing()
    {
        Debug.Log("EndSwing");
        fishSwordCollider.enabled = false;
    }

    public void Swing()
    {
        if (Time.time - lastSwing > swingCooldown)
        {
            lastSwing = Time.time;
            anim.SetTrigger("Swing");
            AudioManager.Instance.PlayClip(Random.Range(4,6));
        }
    }
    private void OnMeteorLand()
    {
        anim.SetTrigger("Idle");
        meteor.Stop();
        meteorLanding.Play();
        FindObjectOfType<CameraShake>().ShakeCamera(0.5f);
        AudioManager.Instance.PlayClip(6);
        landed = true;
        Collider[] cs = Physics.OverlapSphere(transform.position, 1.5f, LayerMask.GetMask("Enemy"));



        // Play particle effect ! $$
        foreach (Collider c in cs)
        {
            Debug.Log(c);
            c.GetComponent<Enemy>().TakeDamage(3);
        }
    }

    public void ChangeToBuild()
    {
        lastStatePos = transform.position;
        state = State.build;
        timer = 0;
    }
    public void ChangeToSlash()
    {
        meteor.Play();
        anim.SetTrigger("Meteor");
        AudioManager.Instance.PlayClip(7);
        landed = false;
        lastStatePos = transform.position;
        state = State.slash;
        timer = 0;
    }
    private Vector3 RotateWithView()
    {
        Vector3 dir = Camera.main.transform.TransformDirection(moveDelta);
        dir.Set(dir.x, 0, dir.z);
        return dir.normalized * moveDelta.magnitude;
    }
}
