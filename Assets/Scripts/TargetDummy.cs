﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDummy : Enemy
{
    [SerializeField] private bool isFirstDummy;
    private Transform childZero;

    protected override void Awake()
    {
        hitpoint = maxHitpoint = 2;
        base.Awake();
        if(isFirstDummy)
            childZero = transform.GetChild(0);
    }

    public void OnBegin()
    {
        childZero.gameObject.SetActive(true);
    }

    protected override void Update()
    {
        base.Update();
        if (isFirstDummy)
            childZero.transform.localScale = Vector3.one * Mathf.Sin(Time.time);
    }

    public override void Destroy()
    {
        if(isFirstDummy)
            GameScene.Instance.ChangeToTowerTriggerTutorial();
        base.Destroy();
    }
}
