﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoSingleton<BuildManager>
{
    [SerializeField] private float tileSize = 1;
    [SerializeField] private Transform previewTile;
    [SerializeField] private GameObject towerPrefab;
    private int selectedX = 0, selectedY = 0;
    private int towerCount = 0;
    private bool isWorking = false;
    private float workTimer;
    private float buildTime = 15.0f;
    private Tower selectedTower;

    private List<Transform> occupiedTiles = new List<Transform>();

    private enum State
    {
        intro = 0,
        build = 1,
        slash = 2,
        meteor = 3,
    }

    private State state;

    private void Start()
    {
        state = State.intro;
    }

    private void Update()
    {
        if (isWorking)
        {
            workTimer += Time.deltaTime;
            selectedTower.transform.localScale = new Vector3(1, workTimer / buildTime, 1);
            if (workTimer > buildTime)
            {
                DoneBuilding();
            }
        }

        switch (state)
        {
            case State.build:
                UpdateBuild();
                break;

            case State.slash:
                UpdateSlash();
                break;

            case State.meteor:
                UpdateMeteor();
                break;

            case State.intro:
            default:
                break;
        }
    }

    public void StartBuilding(Transform hit)
    {
        if (isWorking)
        {
            Debug.Log("Already building");
            return;
        }

        if (occupiedTiles.Contains(hit))
        {
            Debug.Log("Already a tower at here");
            return;
        }

        occupiedTiles.Add(hit);
        GameObject go = Instantiate(towerPrefab);
        go.transform.position = hit.position;
        selectedTower = go.GetComponent<Tower>();
        isWorking = true;
        workTimer = 0;
        towerCount++;

        if (towerCount == 1)
        {
            UIManager.Instance.SetTrigger("Build_Meteor");
        }
    }

    private void DoneBuilding()
    {
        selectedTower.transform.localScale = Vector3.one;
        workTimer = 0;
        isWorking = false;
        selectedTower.NowBuilt();
    }

    private void UpdateBuild()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin,ray.direction * 20,Color.red);
        if (Physics.Raycast(ray, out hit, 20, LayerMask.GetMask("Buildable"), QueryTriggerInteraction.Collide))
        {
            Debug.Log(hit.transform.name);

            previewTile.transform.position = hit.transform.position + Vector3.up * 0.1f;

            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log(string.Format("Tile x:{0} y:{1}", selectedX, selectedY));
                StartBuilding(hit.transform);
            }
        }
    }
    private void UpdateSlash()
    {

    }
    private void UpdateMeteor()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 20, Color.red);
        if (Physics.Raycast(ray, out hit, 20, LayerMask.GetMask("Landable"), QueryTriggerInteraction.Collide))
        {
            previewTile.transform.position = (Vector3.forward * hit.point.z) + hit.transform.position + Vector3.up * 0.01f;

            if (Input.GetMouseButtonDown(0))
            {
                GameScene.Instance.ChangeToSlash();
                Motor.Instance.LandPosition = hit.point;
            }
        }
    }

    public void ChangeToMeteor()
    {
        state = State.meteor;
    }
    public void ChangeToBuild()
    {
        state = State.build;
    }
    public void ChangeToSlash()
    {
        selectedX = selectedY = 0;
        previewTile.transform.position = Vector3.down;
        state = State.slash;
    }
}
