﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarContainer : MonoSingleton<HealthBarContainer>
{
    public List<GameObject> pool = new List<GameObject>();
    public GameObject healthBarPrefab;
    public Transform healthBarContainer;

    public HealthBar GetHealthBar()
    {
        GameObject go = pool.Find(x => !x.activeSelf);

        if (go == null)
            go = Instantiate(healthBarPrefab, healthBarContainer);

        return go.GetComponent<HealthBar>();
    }
}
