﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarContainer : MonoSingleton<ProgressBarContainer>
{
    public List<GameObject> pool = new List<GameObject>();
    public GameObject progressBarPrefab;

    public ProgressBar GetProgressBar()
    {
        GameObject go = pool.Find(x => !x.activeSelf);

        if (go == null)
            go = Instantiate(progressBarPrefab, transform);

        return go.GetComponent<ProgressBar>();
    }
}
