﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScene : MonoSingleton<GameScene>
{
    /*
     * 0 -> Build State
     * 1 -> Slash State
     * 2 -> Death State
     * 3 -> Intro State
     * 4 -> Tutorial State
     * 5 -> Slash Tutorial
     * 6 -> Tower Trigger
     * 7 -> Meteor
    */
    [SerializeField] private List<BaseGameState> allStates;
    [SerializeField] private BaseGameState state;

    private bool firstTimeSlash = true;
    private bool firstTimeBuild = true;

    private void Start()
    {
        ChangeToIntro();
    }
    private void Update()
    {
        state.UpdateState();
    }

    private void LaunchGame()
    {
        ChangeToBuild();
        SpawnManager.Instance.OnGameStart();
    }

    public void ChangeToBuild()
    {
        if (firstTimeBuild)
        {
            firstTimeBuild = false;
            LaunchGame();
        }

        ChangeState(allStates[0]);
    }
    public void ChangeToSlash()
    {
        if (firstTimeSlash)
        {
            ChangeState(allStates[5]);
            firstTimeSlash = false;
        }
        else
            ChangeState(allStates[1]);
    }
    public void ChangeToMeteor()
    {
        ChangeState(allStates[7]);
    }
    public void ChangeToDeath()
    {
        ChangeState(allStates[2]);
    }
    public void ChangeToIntro()
    {
        ChangeState(allStates[3]);
    }
    public void ChangeToTutorial()
    {
        ChangeState(allStates[4]);
    }
    public void ChangeToTowerTriggerTutorial()
    {
        ChangeState(allStates[6]);
    }
    private void ChangeState(BaseGameState s)
    {
        Debug.Log("Changing to state " + s.GetType().Name);

        state.Destruct();
        state = s;
        state.Construct();
    }
}
