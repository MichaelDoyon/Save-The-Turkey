﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTopDown : MonoBehaviour
{
    [SerializeField] private float scrollSpeed = 15.0f;
    [SerializeField] private float currZ = 0;
    private float deadzone = 50;

    private float clampMin = 3.7f;
    private float clampMax = 20f;

    private void Update()
    {
        Vector3 mp = Input.mousePosition;

        if (mp.y <= 100)
        {
            currZ -= scrollSpeed * Time.deltaTime;
        }

        if (mp.y >= Screen.height - 100)
        {
            currZ += scrollSpeed * Time.deltaTime;
        }

        currZ = Mathf.Clamp(currZ, clampMin, clampMax);

        transform.position = Vector3.forward * currZ;
    }
}
