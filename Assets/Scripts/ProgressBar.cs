﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    private float timer;
    private float duration = 2;
    private bool isActive;
    private Image fill;
    private Transform target;
    public Vector3 offset;

    private void Awake()
    {
        fill = transform.GetChild(0).GetComponent<Image>();
    }

    private void Update()
    {
        if (isActive)
        {
            timer += Time.deltaTime;
            fill.fillAmount = (float)timer / (float)duration;
            Debug.Log(offset);
            transform.position = Camera.main.WorldToScreenPoint(target.position + offset);
            if (timer >= duration)
            {
                fill.fillAmount = 1;
                ActionCompleted();
            }
        }
    }

    public void StartAction(float duration, Transform target, Vector3 offset)
    {
        this.duration = duration;
        this.offset = offset;
        this.target = target;
        fill.fillAmount = 0;
        gameObject.SetActive(true);
        timer = 0;
        transform.position = Camera.main.WorldToScreenPoint(target.position + offset);
        isActive = true;
    }

    public void ActionCompleted()
    {
        isActive = false;
        gameObject.SetActive(false);
    }
}
