﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private Transform fill;
    public Transform target;
    public Vector3 offset = Vector3.up;

    private void Awake()
    {
        fill = transform.GetChild(0);
    }

    private void Update()
    {
        transform.position = Camera.main.WorldToScreenPoint(target.position + offset);
    }

    public void Reset()
    {
        gameObject.SetActive(true);
    }

    public void UpdateHealth(int hp,int mhp)
    {
        float ratio = (float)hp / (float)mhp;
        ratio = Mathf.Clamp(ratio, 0, 1);
        fill.localScale = new Vector3(ratio, 1, 1);
    }
}
