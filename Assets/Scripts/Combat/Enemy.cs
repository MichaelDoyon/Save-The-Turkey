﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator anim;
    private HealthBar hpBar;
    private CharacterController controller;
    private bool isLaunched = false;
    public int hitpoint = 2;
    public int maxHitpoint = 2;

    protected virtual void Awake()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        hpBar = HealthBarContainer.Instance.GetHealthBar();
        hpBar.target = transform;
    }

    protected virtual void Update()
    {
        if (!isLaunched)
            return;

        controller.Move(Vector3.forward * Time.deltaTime);
    }

    public void Launch()
    {
        isLaunched = true;
        gameObject.SetActive(true);
        transform.position = Vector3.zero;
        hpBar.Reset();
    }

    public virtual void Destroy()
    {
        isLaunched = false;
        gameObject.SetActive(false);
        transform.position = Vector3.zero;
        hpBar.gameObject.SetActive(false);
        AudioManager.Instance.PlayClip(2);
        hpBar.UpdateHealth(1, 1);
    }

    public virtual void TakeDamage(int amn)
    {
        hitpoint -= amn;
        anim.SetTrigger("Hit");
        hpBar.UpdateHealth(hitpoint, maxHitpoint);

        if (hitpoint <= 0)
        {
            Destroy();
        }
        else
        {
            AudioManager.Instance.PlayClip(1);
        }

    }
}
