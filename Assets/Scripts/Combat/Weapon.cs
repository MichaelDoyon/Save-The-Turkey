﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private int damage = 1;

    public void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "Enemy":
                other.GetComponent<Enemy>().TakeDamage(damage);
                break;
            case "Tower":
                // Play fx $$
                // other.GetComponent<Tower>().LaunchAttack();
                break;
        }
    }
}
